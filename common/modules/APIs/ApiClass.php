<?php

namespace common\modules\apis;

use yii\base\Exception;

abstract class ApiClass extends \yii\db\ActiveRecord
{
    protected $connection_data = [];

    protected $data_params = [];

    protected $url;

    protected $from = '2017-02-13';
    protected $to = '2017-02-13';

    private function authdata($dat)
    {

        $data = '';
        if(count($dat) !== 0) {
            foreach ($dat as $key=>$datum)
            {
                $data .= '&'.$datum['param_name'].'='.$datum['param_value'];
            }
        }
        return $data;
    }

    public function parse($url, $connection_data){
        $auth = $this->authdata($connection_data);
        $curl_options = array(
            CURLOPT_URL => "$url&from=$this->from&to=$this->to$auth",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CONNECTTIMEOUT => 5
        );

        $curl = curl_init();
        curl_setopt_array( $curl, $curl_options );
        $result = curl_exec( $curl );
        $status = curl_getinfo($curl)['http_code'];
        $message = $this->_status($status);
        $result = (array) json_decode($result);
        return ['status' => $message, 'data' => $result];
    }

    private function _status($code) {
        $status = array(
            200 => 'OK',
            403 => 'Access denied!',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[500];
    }
}