<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Api]].
 *
 * @see \common\models\Api
 */
class ApiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Api[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Api|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
