<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="modal show" style="position: relative;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Available options</h5>
                </div>
                <div class="modal-body text-center">
                    <p>
                        <a href="/site/settings"><button type="button" class="btn btn-secondary">Settings</button></a>
                        <a href="/site/livescore"><button type="button" class="btn btn-primary">Livescore</button></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
