<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <?php $form = \yii\widgets\ActiveForm::begin([
        'id'                        => 'tabular-form',
        'enableAjaxValidation'      => false,
        'enableClientValidation'    => false,
        'validateOnChange'          => false,
        'validateOnSubmit'          => false,
        'validateOnBlur'            => false,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    <div class="modal show" style="position: relative; height: 500px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="row">
                    <label style="float: left; margin-left: 30px;">Current API</label>
                    <?= \yii\helpers\Html::dropDownList('api', 'null', \yii\helpers\ArrayHelper::map($apis, 'id', 'name'), [
                            'prompt' => 'Select API',
                            'id' => 'api_change',
                            'style' => 'width:50%; float: right; margin-right: 30px;'
                    ]) ?>
                    </div>
                    <?php
                        $intervals = [
                                5 => '5m',
                                10 => '10m',
                                15 => '15m',
                                30 => '30m',
                                60 => '60m'
                        ];
                    ?>
                    <div class="row">
                        <label style="float: left; margin-left: 30px;">Update interval</label>
                        <?= \yii\helpers\Html::dropDownList('api', $apis, $intervals, [
                            'id' => 'interval',
                            'style' => 'width:50%; float: right; margin-right: 30px;'
                        ]) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <p>
                        <button type="button" class="btn btn-primary" id="ajax">Save settings</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

</div>
