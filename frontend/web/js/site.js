$(document).ready(function () {
    $(document.body).on('change', '#api_change', function () {

        var val = $('#api_change').val();
        console.log(val.length);
        if (val.length == 0) {
            $('.ajax').remove();
            return false;
        }
        $.ajax({
            type: "POST",
            url: "api-change",
            data: {id : val},

            success: function(data){
                // console.log(data);
                // console.log($('#interval option[value='+data['api']['update_interval']+']'));
                $('#interval option[value='+data['api']['update_interval']+']').attr('selected', true);
                $('.ajax').remove();
                $(".modal-body").append(data['html']);
            }
        });
        return false;
    });

    $('#ajax').on('click', function () {
        $.ajax({
            type: "POST",
            url: "api-save",
            data: {
                api : $('#api_change').val(),
                time : $('#interval').val(),
                param : $('.param').val(),
                name : $('.param').attr('id'),
            },

            success: function(data){
                if(data == 1){
                    console.log($('button#ajax'));
                    $('#ajax').css("background-color", "#5cb85c");
                    $('#ajax').html('Saved');
                    // var elem = $('#ajax');
                    //
                    // setTimeout(function (elem) {
                    //     elem.css("background-color", "#337ab7")
                    // }, 1000);
                    // setTimeout($('#ajax').html('Save settings'), 1000);

                    // $('button#ajax').removeClass('btn-primary').addClass('btn-success');
                }
            }
        });
    })
});