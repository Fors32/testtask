<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api`.
 */
class m170924_160449_create_api_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('api', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'request_uri' => $this->text()->notNull(),
            'update_interval' => $this->integer(),
            'params' => $this->text(),
            'last_response' => $this->text(),
            'is_active' => $this->integer()->defaultValue(0),
            'refresh_timestamp' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('api');
    }
}
