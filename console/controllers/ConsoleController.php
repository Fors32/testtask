<?php
namespace console\controllers;

use yii\console\Controller;
use yii2tech\crontab\CronJob;
use yii2tech\crontab\CronTab;

class ConsoleController extends Controller {
    public function actionIndex()
    {
        $api = \common\models\Api::find()->where(['is_active' => 1])->one();

        $cronJob = new CronJob();

        $cronJob->min = $api->update_interval;
        $cronJob->hour = '0';
        $cronJob->command = 'yii console';

        $cronTab = new CronTab();

        $cronTab->setJobs([
            $cronJob
        ]);
        $cronTab->apply();
        $parse = $api->parse();
        return $parse;
    }

    public function actionTest()
    {
        return 'test';
    }
}