<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model common\models\Api */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-form">


    <?php $form = ActiveForm::begin([
        'id'                        => 'tabular-form',
        'enableAjaxValidation'      => true,
        'enableClientValidation'    => false,
        'validateOnChange'          => false,
        'validateOnSubmit'          => true,
        'validateOnBlur'            => false,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'request_uri')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'update_interval')->textInput() ?>

<!--    --><?//= $form->field($model, 'params')->textarea(['rows' => 6]) ?>
    <?=
    $form->field($model, 'params')->widget(MultipleInput::className(), [
        'max' => 4,
        'columns' => [
            [
                'name'  => 'param_name',
                'title' => 'Name',
                'value' => function($data) {
                    return $data['param_name'];
                },
            ],
            [
                'name'  => 'param_value',
                'title' => 'Value',
                'value' => function($data) {
                    return $data['param_value'];
                },
            ],
        ]
    ]);
    ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
