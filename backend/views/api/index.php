<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\query\ApiQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Api', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'request_uri:ntext',
            'update_interval',
            'refresh_timestamp:datetime',
            [
                'attribute' => 'params',
                'format' => 'html',
                'value' => function($model) {
                $html = '<ul>';
                    if (!empty($model->params)) {
                        foreach (unserialize($model->params) as $key=>$param){
                          $html.= '<li>'.$param["param_name"].': '.$param["param_value"].'</li>';
                        }
                    }
                    $html .= '</ul>';
                    return Html::decode($html);
                }
            ],
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
